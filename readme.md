# Recipes


## Contents


* [Lobster Mac n' Cheese](https://github.com/joshnesbitt/recipes/blob/master/recipes/lobster-mac-n-cheese.md)
* [Slow Cooker Pulled Pork](https://github.com/joshnesbitt/recipes/blob/master/recipes/slow-cooker-pulled-pork.md)


## What is this?


This is a collection of recipes I've gathered over the years. At the moment there's only a few, so I'm not that good at it. I do however like [cooking a lot](http://instagram.com/joshnesbitt).


## Preview


_Lobster mac n' cheese..._

![Lobster mac n' cheese](https://github.com/joshnesbitt/recipes/blob/master/images/lobster-mac-n-cheese-1.jpg)
![Lobster mac n' cheese](https://github.com/joshnesbitt/recipes/blob/master/images/lobster-mac-n-cheese-2.jpg)

_BBQ'ed leg of lamb..._

![BBQ'ed leg of lamb](https://github.com/joshnesbitt/recipes/blob/master/images/bbqed-leg-of-lamb.jpg)

_Chunky avocado and bacon salad with pine nuts..._

![Chunky avocado and bacon salad with pine nuts](https://github.com/joshnesbitt/recipes/blob/master/images/chunky-avocado-and-bacon-salad-with-pine-nuts.jpg)

_Shrimp and chorizo fusilli lunghi col buco with grilled squid..._

![Shrimp and chorizo fusilli lunghi col buco with grilled squid](https://github.com/joshnesbitt/recipes/blob/master/images/shrimp-and-chorizo-fusilli-lunghi-col-buco-with-grilled-squid.jpg)

_Sesame and soy pork..._

![Sesame and soy pork](https://github.com/joshnesbitt/recipes/blob/master/images/sesame-and-soy-pork.jpg)

_"Proper" pork belly..._

!["Proper" pork belly](https://github.com/joshnesbitt/recipes/blob/master/images/proper-pork-belly.jpg)

_Beef strips with Moroccan cous cous and tzatziki..._

![Beef strips with Moroccan cous cous and tzatziki](https://github.com/joshnesbitt/recipes/blob/master/images/beef-strips-with-moroccan-cous-cous-and-tzatziki.jpg)

_Slow cooked pork belly (before)..._

![Slow cooked pork belly (before)](https://github.com/joshnesbitt/recipes/blob/master/images/slow-cooked-pork-belly-before.jpg)

_Frank (in a tank)..._

![Frank](https://github.com/joshnesbitt/recipes/blob/master/images/frank-in-a-tank.jpg)


## License


The MIT License (MIT)

Copyright (c) [2014] [Josh Nesbitt]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
