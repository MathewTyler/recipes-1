# Jamie Oliver's Lobster Mac n' Cheese


![Lobster mac n' cheese](https://github.com/joshnesbitt/recipes/blob/master/images/lobster-mac-n-cheese-1.jpg)

This recipe is heavily inspired by a recipe from Jamie Oliver's comfort food series. I'd link to it, but it's since been taken down to promote the launch of his [recipe book](http://www.jamieoliver.com/comfortfood).


## Ingredients


* 4 x 1kg live lobsters
* 2 x lemons
* 2 large onions
* 6 cloves of garlic
* 70g unsalted butter
* 1 pinch of saffron
* 3 x anchovy fillets
* 1 large glass of white Burgundy
* 60g plain flour
* 1.4 litres of semi skimmed milk
* 2 teaspoons of English mustard
* 600g Amori pasta
* 80g mature cheddar
* 80g Gruyere
* Olive bread sticks (for topping)
* Parmesan (for topping and serving)


## Method


* Place the live lobsters into the freezer
* Take a large pot and bring to the boil. Season with salt and 2 squeezed lemons
* After 25 - 30 minutes, take the lobsters out the freezer (one by one), spike (down the center of the underbelly) and place into the pot
* Leave to cook for 10 minutes once water begins to boil again
* Once the lobsters are cooked, place them in a bowl to steam dry for later
* Put the pasta on to boil in salted water, take out just before Al Dente
* In a large pot, fry off the onions and garlic until golden brown
* Add the white Burgundy and simmer off
* Add the anchovy fillets, butter and mustard
* Once the butter has melted, add the flour and mix into a paste
* Now gradually add the milk until you have a smooth bechamel sauce
* Add a pinch of saffron
* Add the cheddar and Gruyere, melt into sauce
* Ensure the sauced is completely mixed together and smooth, then add the pasta
* Now break the lobster up, separate the tail meat from the rest of the body and extract any remaining meat
* Place the tails to one side, chop the rest of the meat up and mix into the sauce
* Dish the mixed pasta into 4 serving pots
* Crumble the olive bread sticks into a fine crumb and dust over the top of each pot
* Sprinkle with Parmesan and place in the oven for 25 mins
* Take the pots from the oven, add the tails to the pots for presentation and return to bake for a further 10 mins (it might be useful to split the lobster tails underneath before doing so to make it easier for guests to take the meat out later)
* Remove from oven and serve on a board with the lobster heads for presentation


## Notes


* Some people like to just chill the lobsters before plunging them into the boiling water, but following quite [a bit of research](http://www.animalaid.org.uk/h/n/campaigns/vegetarianism/all/522) I prefer freezing for 25 minutes, spiking and then placing them in the water. It's a matter of preference, but I believe this to be more ethical and respectful to the animal
* As the article above mentions, spiking does take some practice, so take care when doing so as you don't want to a) cause more pain to the animal and b) slice your fingers off
* Once each lobster is placed into the pot, have a helper place the lid back on the pot. Due to a decentralised nervous system the lobsters sometimes still move when placed in and this can cause boiling water to splash from the pan
* There's a dark meat inside the lobster heads which some people enjoy, so it's worth saving the heads to serve up at the end
