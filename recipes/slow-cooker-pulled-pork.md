# Slow Cooker Pulled Pork


## Ingredients


### Meat & Rub


* Boned pork shoulder - 1 large cut
* Brown sugar - 50g
* Salt flakes - 10g
* Pepper - 10g
* Olive oil (extra virgin) - Drizzle
* Red onions - 2 large


### Sauce


* Brown sugar - 50g
* Smoked paprika - 1 or 2 teaspoons (based on preference, it can be quite strong)
* Passata / thick tomato juice - 1 jar
* Tabasco sauce (chipotole, smokey) - 1 tsp
* Cayenne pepper - 1 tsp
* Chillies (or a scotch bonnet if you prefer) - 1 tsp


## Method


* Heat the slow cooker up before hand, it needs to be on a low heat
* Caramelise the red onions in the olive oil until soft (some browning is good). Once done, lay them at the bottom of the slow cooker as a bottom layer
* Take the pork, remove and skin and excess fat. You want a layer of fat on top, but it's not going to crisp up like a normal roast so you don't want too much. Rub the salt, pepper and sugar all over the pork
* In a saucepan, add the passata, paprika, pepper and brown sugar, you want the right balance of heat to sweetness. Once you've added these, add the chillies and Tabasco in moderation, the Tabasco is important for the smokey flavour. Reduce the sauce until it's thickened down nicely, then remove from the heat
* Pour some of the sauce on the bed of onions, then place the pork on top
* Pour the remaining sauce over the pork and place the lid on. You'll want to leave a third of the pot free of sauce to allow room for juices to form on top
* Cook for 24 hours
* Once cooked, take the pork out and rest for 15 mins with foil over it
* In the mean time, place the remaining sauce and fat into a saucepan and reduce
* Place the pork in a large tray and pull apart with two forks, leaving strands of pork
* Add the sauce back to the tray to rehydrate the pork and give it a juicy consistency
* Get stuck in


## Notes


* The measures of the glaze ingredients differs depending on your sensitivity to spice etc, so build it up until you're happy with it, like a good pasta sauce
* The pork needs to be cooked for _at least_ 16 hours
* The pork will produce a lot of juice and fat, which will need reducing after
